console.log("Hello World");

	let human = {
	firstName: 'Nhiko',
	lastName: ' Nicodemus',
	age: 18,
	
	Hobbies: ['Playing, Reading, Coding'],

	workAddress: {
		houseNumber: '19',
		street: 'Brooklyn',
		city: 'Deerfield',
		state: 'Virginia',
	}
}

let work = Object.values(human.workAddress);

console.log("My first name is " + human.firstName)

console.log("My last name is " + human.lastName)

console.log("My full name is " + human.firstName + human.lastName)

console.log("My current age is " + human.age)

console.log("I work at " + work.join(", ") + ".");
